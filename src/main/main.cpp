#include <adapters/adapters_factory.hpp>

int main()
{
    auto config_ldr = AdaptersFactory::makeConfigProvider("env");
    auto config = config_ldr->config();

    auto http_srv = AdaptersFactory::makeHTTPServer();
    auto author_cnt = AdaptersFactory::makeHTTPWhoamiController(*http_srv);

    http_srv->start(config.http.host, config.http.port, config.http.threads);
    http_srv->wait();

    return 0;
}
