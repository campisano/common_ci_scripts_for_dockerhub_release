#include <adapters/adapters_factory.hpp>

#include <sstream>
#include <stdexcept>
#include "controllers/httplib_http_whoami_controller.hpp"
#include "drivers/http/httplib_http_server.hpp"
#include "drivers/config/env_config_provider.hpp"

AdaptersFactory::AdaptersFactory()
{
}

std::unique_ptr<ConfigProvider> AdaptersFactory::makeConfigProvider(
    const std::string & _source)
{
    if(_source == "env")
    {
        return std::unique_ptr<ConfigProvider>(new EnvConfigProvider());
    }

    std::stringstream msg;
    msg << "Specified config source is invalid: " << _source;
    throw std::runtime_error(msg.str());
}

std::unique_ptr<HTTPServer> AdaptersFactory::makeHTTPServer()
{
    return std::unique_ptr<HTTPServer>(new HttplibHTTPServer());
}

std::unique_ptr<HTTPController> AdaptersFactory::makeHTTPWhoamiController(
    HTTPServer & _server)
{
    return std::unique_ptr<HTTPController>(
               new HttplibHTTPWhoamiController(_server));
}
