#ifndef HTTPLIB_HTTP_WHOAMI_CONTROLLER__HPP__
#define HTTPLIB_HTTP_WHOAMI_CONTROLLER__HPP__

#include <adapters/controllers/http_controller.hpp>
#include <adapters/drivers/http_server.hpp>

#include <httplib.h>

class HttplibHTTPWhoamiController : public HTTPController
{
public:
    explicit HttplibHTTPWhoamiController(HTTPServer & _server);
    HttplibHTTPWhoamiController(const HttplibHTTPWhoamiController &) = delete;
    HttplibHTTPWhoamiController(HttplibHTTPWhoamiController &&) = default;
    virtual ~HttplibHTTPWhoamiController();

    HttplibHTTPWhoamiController & operator=(
        const HttplibHTTPWhoamiController &) = delete;
    HttplibHTTPWhoamiController & operator=(
        HttplibHTTPWhoamiController &&) = default;

public:
    void whoami(
        const httplib::Request &,
        httplib::Response &);
};

#endif
