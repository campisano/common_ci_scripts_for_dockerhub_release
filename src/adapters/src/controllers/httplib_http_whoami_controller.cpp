#include "httplib_http_whoami_controller.hpp"

#include <stdexcept>

#include "httplib_utils.hpp"
#include "../drivers/http/httplib_http_server.hpp"

HttplibHTTPWhoamiController::HttplibHTTPWhoamiController(HTTPServer & _server)
{
    HttplibHTTPServer & server = dynamic_cast<HttplibHTTPServer &>(_server);
    server.route(
        R"(/(.*))",
        "all",
        std::bind(
            &HttplibHTTPWhoamiController::whoami,
            this,
            std::placeholders::_1,
            std::placeholders::_2));
}

HttplibHTTPWhoamiController::~HttplibHTTPWhoamiController()
{
}

void HttplibHTTPWhoamiController::whoami(
    const httplib::Request & _request,
    httplib::Response & _response)
{
    try
    {
        setJSONResponse(
            _response, 200,
        {
            {"remote_addr", _request.remote_addr},
            {"remote_port", _request.remote_port},
            {"method", _request.method},
            {"path", _request.path},
            {"target", _request.target},
            {"version", _request.version},
            {"params", _request.params},
            {"headers", _request.headers},
            {"body", _request.body}
        });
    }
    catch(const std::exception & _exc)
    {
        setJSONResponse(
            _response, 500,
        {{"message", "Internal server error"}});
    }
}
