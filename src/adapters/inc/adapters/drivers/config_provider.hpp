#ifndef CONFIG_PROVIDER__HPP__
#define CONFIG_PROVIDER__HPP__

#include "config.hpp"

class ConfigProvider
{
public:
    virtual ~ConfigProvider() = default;

public:
    virtual Config config() = 0;
};

#endif
