#ifndef CONFIG__HPP__
#define CONFIG__HPP__

#include <string>

struct Config
{
    struct Http
    {
        std::string host;
        int port;
        int threads;
    } http;
};

#endif
