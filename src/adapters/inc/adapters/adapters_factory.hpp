#ifndef ADAPTERS_FACTORY__HPP__
#define ADAPTERS_FACTORY__HPP__

#include <adapters/controllers/http_controller.hpp>
#include <adapters/drivers/config_provider.hpp>
#include <adapters/drivers/http_server.hpp>
#include <memory>

class AdaptersFactory
{
private:
    explicit AdaptersFactory();

public:
    static std::unique_ptr<ConfigProvider> makeConfigProvider(
        const std::string & _driver);

    static std::unique_ptr<HTTPServer> makeHTTPServer();
    static std::unique_ptr<HTTPController> makeHTTPWhoamiController(
        HTTPServer & _server);
};

#endif
