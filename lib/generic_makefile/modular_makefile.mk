# mandatory vars:

# binary_name			:= # example: foo.so
# type				:= # possible values: {lib, exec}



# optional vars:
default_targets			?= format debug run

include_paths			?= inc
library_paths			?=

source_extension		?= .cpp
source_paths			?= src
libs_to_link			?=

main_source_paths		?=

test_source_paths		?=
test_add_libs_to_link		?=

output_folder			?= build
DESTDIR				?=
PREFIX				?= /usr/local

format_dirs			?= inc src
format_extensions		?= .hpp .cpp

subcomponent_paths		?=



###############################################################################

# define defaults make options
MAKEFLAGS			= --environment-overrides --no-builtin-rules --warn-undefined-variables

# defining variable to avoid warning
MAKECMDGOALS			?=





####################
# check mandatory variables

ifndef binary_name
$(error var 'binary_name' is mandatory)
endif

ifndef type
$(error var 'type' is mandatory)
endif

ifeq ($(type), exec)
output_subfolder		:= bin
else ifeq ($(type), lib)
output_subfolder		:= lib
else
$(error wrong value for var 'type', valid are {exec,lib})
endif





####################
# define O.S. specific commands

ifdef COMSPEC
	CP			?= copy
	MKDIR			?= md
	MV			?= move
	RM			?= del
	RMALL			?= rd /s /q
else
	CP			?= cp
	MKDIR			?= mkdir -p
	MV			?= mv -f
	RM			?= rm -f
	RMALL			?= rm -rf
endif





####################
# define basic flags

# for C pre-processor: add include paths and generate *.d dependency files in compile time
CPPFLAGS_COMMON			:= $(addprefix -I,$(include_paths)) -MMD -MP

# for C++ compiler
CXXFLAGS_COMMON			:= -pthread -pipe -std=c++11 -fexceptions -pedantic -Wall -Wextra -Wshadow -Wnon-virtual-dtor

# for compilers invoking the linker:
LDFLAGS_COMMON			:= -pthread $(addprefix -L,$(library_paths)) $(addprefix -l,$(libs_to_link))





####################
# define specific flags
# see
# https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html
# https://gcc.gnu.org/onlinedocs/gcc/Debugging-Options.html
# https://gcc.gnu.org/onlinedocs/gcc/Link-Options.html

# use optimization, remove all symbol table
CXXFLAGS_RELEASE		:= -O2 -s
LDFLAGS_RELEASE			:=

# use debug optimization, increase debug level to 3, do not expand inline functions, keep frame pointer to use linux 'prof' tool
CXXFLAGS_DEBUG			:= -Og -g3 -fno-inline -fno-omit-frame-pointer
# add all symbols, not only used ones, to the dynamic symbol table
LDFLAGS_DEBUG			:= -rdynamic

# compile code instrumented for coverage analysis
CXXFLAGS_TEST			:= --coverage
# link code instrumented for coverage analysis
LDFLAGS_TEST			:= $(addprefix -l,$(test_add_libs_to_link)) --coverage

CXXFLAGS_LIB			:= -fPIC
LDFLAGS_LIB			:= -shared

CXXFLAGS_STATIC			:=
LDFLAGS_STATIC			:= -static -static-libgcc -static-libstdc++

# add gprof tool info
CXXFLAGS_PROFILE		:= -g -pg
# add all symbols, not only used ones, to the dynamic symbol table
LDFLAGS_PROFILE			:= -rdynamic -pg





####################
# define build sources

sources_base			:= $(foreach tmp_path,$(source_paths),$(wildcard $(tmp_path)/*$(source_extension)))
sources_main			:= $(foreach tmp_path,$(main_source_paths),$(wildcard $(tmp_path)/*$(source_extension)))
sources_test			:= $(foreach tmp_path,$(test_source_paths),$(wildcard $(tmp_path)/*$(source_extension)))





####################
# define targets

build_targets	 		:= $(output_folder)/$(output_subfolder)/$(binary_name)
objects_main			:= $(foreach source,$(strip $(sources_base) $(sources_main)),$(output_folder)/$(output_subfolder)/$(source).o)


# add test target sources when exists
ifeq ($(strip $(sources_test)),)
build_targets_test		:=
objects_test			:=
else
build_targets_test		:= $(output_folder)/test/$(binary_name)
objects_test			:= $(foreach source,$(strip $(sources_base) $(sources_test)),$(output_folder)/test/$(source).o)
endif



.PHONY: all
all: default_targets

.PHONY: default_targets
default_targets: $(default_targets)

.PHONY: release
release: CXXFLAGS_BY_SCOPE=$(CXXFLAGS_RELEASE)
release: LDFLAGS_BY_SCOPE=$(LDFLAGS_RELEASE)
release: submakefiles $(build_targets)

.PHONY: debug
debug: CXXFLAGS_BY_SCOPE=$(CXXFLAGS_DEBUG)
debug: LDFLAGS_BY_SCOPE=$(LDFLAGS_DEBUG)
debug: submakefiles $(build_targets)

.PHONY: test
test: submakefiles $(build_targets_test)



####################
# main executable targets

$(output_folder)/bin/$(binary_name): $(objects_main)
	@echo "   -> *.o -o $@"
	@$(MKDIR) $(dir $@)
	@$(CXX) -Wl,--no-as-needed -o $@ $(objects_main) $(LDFLAGS_COMMON) $(LDFLAGS_BY_SCOPE)

$(output_folder)/bin/%.o: %
	@echo " * $< -o $@"
	@$(MKDIR) $(dir $@)
	@$(CXX) -o $@ -c $< $(CPPFLAGS_COMMON) $(CXXFLAGS_COMMON) $(CXXFLAGS_BY_SCOPE)



####################
# library targets

$(output_folder)/lib/$(binary_name): $(objects_main)
	@echo "   -> *.o -o $@"
	@$(MKDIR) $(dir $@)
	@$(CXX) -Wl,--no-as-needed -o $@ $(objects_main) $(LDFLAGS_COMMON) $(LDFLAGS_BY_SCOPE) $(LDFLAGS_LIB)

$(output_folder)/lib/%.o: %
	@echo " * $< -o $@"
	@$(MKDIR) $(dir $@)
	@$(CXX) -o $@ -c $< $(CPPFLAGS_COMMON) $(CXXFLAGS_COMMON) $(CXXFLAGS_BY_SCOPE) $(CXXFLAGS_LIB)



####################
# test targets

$(output_folder)/test/$(binary_name): $(objects_test)
	@echo "   -> *.o -o $@"
	@$(MKDIR) $(dir $@)
	@$(CXX) -Wl,--no-as-needed -o $@ $(objects_test) $(LDFLAGS_COMMON) $(LDFLAGS_DEBUG) $(LDFLAGS_TEST)

$(output_folder)/test/%.o: %
	@echo " * $< -o $@"
	@$(MKDIR) $(dir $@)
	@$(CXX) -o $@ -c $< $(CPPFLAGS_COMMON) $(CXXFLAGS_COMMON) $(CXXFLAGS_DEBUG) $(CXXFLAGS_TEST)



NODEPS				:= clean run test
ifeq (0, $(words $(findstring $(MAKECMDGOALS), $(NODEPS))))
-include $(patsubst %$(source_extension),$(output_folder)/%$(source_extension).d,$(strip $(sources_base) $(sources_test) $(sources_main)))
-include $(patsubst %$(source_extension),$(output_folder)/test/%$(source_extension).d,$(strip $(sources_base) $(sources_test) $(sources_main)))
endif





####################
# runnable targets



.PHONY: run
run:
ifeq ($(type), exec)
	LD_LIBRARY_PATH=$(subst $(subst ,, ),:,$(library_paths)) $(output_folder)/bin/$(binary_name)
endif



.PHONY: check
check: submakefiles
ifneq ($(strip $(sources_test)),)
	LD_LIBRARY_PATH=$(subst $(subst ,, ),:,$(library_paths)) $(output_folder)/test/$(binary_name)
endif



.PHONY: gcov
gcov: submakefiles
	$(foreach source,$(strip $(sources_base) $(sources_main)),gcov -p -r -o $(output_folder)/test/$(source).o $(source); )



.PHONY: ddd nemiver
ddd nemiver:
ifeq ($(type), exec)
	LD_LIBRARY_PATH=$(subst $(subst ,, ),:,$(library_paths)) $@ $(output_folder)/bin/$(binary_name)
endif

.PHONY: perf
perf:
ifeq ($(type), exec)
	@echo "# Makefile note:"
	@echo "# you may need to setup kernel permissions, use 'cat /proc/sys/kernel/perf_event_paranoid; sudo sh -c \"echo 3 >/proc/sys/kernel/perf_event_paranoid\"')"
	@echo "# use 'perf report -g graph -s period,comm,dso,symbol' to analyze using TUI interface"
	@echo "# or  'perf script | c++filt | gprof2dot.py -f perf | dot -Tpng -o perf.png' to analyze using graphviz"
	@echo ""
	LD_LIBRARY_PATH=$(subst $(subst ,, ),:,$(library_paths)) perf record -q -e cycles -g --call-graph fp -- $(output_folder)/bin/$(binary_name) || true
	test -f perf.data && chmod a+r perf.data && perf report -g graph -s period,comm,dso,symbol || true
endif



# see https://sourceware.org/gdb/current/onlinedocs/gdb.html/Mode-Options.html
.PHONY: egdb
egdb:
ifeq ($(type), exec)
	LD_LIBRARY_PATH=$(subst $(subst ,, ),:,$(library_paths)) emacs --execute '(gdb "gdb --silent --nw --interpreter mi --ex \"break main\" --ex \"run\" $(output_folder)/bin/$(binary_name)")'
endif





####################
# other targets



.PHONY: format
format: submakefiles
ifneq ($(strip $(sources_base)),)
	@type astyle > /dev/null \
&& astyle --options=none --quiet --style=allman \
--indent=spaces=4 --lineend=linux --align-pointer=middle \
--pad-oper --pad-comma --unpad-paren \
--add-brackets --convert-tabs --max-code-length=80 \
--suffix=none \
$(shell find $(format_dirs) -type f \( -name \*$(word 1, $(format_extensions)) $(foreach ext,$(wordlist 2, $(words $(format_extensions)), $(format_extensions)), -o -name \*$(ext)) \)) \
|| true
endif



.PHONY: clean
clean: submakefiles
	@$(RM) $(objects_main) $(objects_main:.o=.gcno)
	@$(RM) $(objects_test) $(objects_test:.o=.gcno) $(objects_test:.o=.gcda)
	@$(RM) $(output_folder)/bin/$(binary_name) $(output_folder)/lib/$(binary_name) $(output_folder)/test/$(binary_name)



.PHONY: install
install: submakefiles
	$(MKDIR) $(DESTDIR)$(PREFIX)/$(output_subfolder)
	$(CP) $(output_folder)/$(output_subfolder)/$(binary_name) $(DESTDIR)$(PREFIX)/$(output_subfolder)



.PHONY: submakefiles
submakefiles:
	@for tmp_path in $(subcomponent_paths); do \
	echo "----- $$tmp_path -----"; \
	$(MAKE) -C $$tmp_path $(MAKECMDGOALS) PREFIX=$(PREFIX) || exit; \
	echo; \
	done;
