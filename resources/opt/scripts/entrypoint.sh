#!/usr/bin/env bash

set -o errexit -o nounset -o pipefail

if test -f /tmp/build.date
then
    echo -en "Build at "
    cat /tmp/build.date
    echo
fi



exec "$@"
