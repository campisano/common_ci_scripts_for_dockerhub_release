[![Build Status](https://gitlab.com/campisano/common_ci_scripts_for_dockerhub_release/badges/master/pipeline.svg "Build Status")](https://gitlab.com/campisano/common_ci_scripts_for_dockerhub_release/-/pipelines)
[![Sonar Coverage](https://sonarcloud.io/api/project_badges/measure?project=common_ci_scripts_for_dockerhub_release&metric=coverage "Sonar Coverage")](https://sonarcloud.io/dashboard?id=common_ci_scripts_for_dockerhub_release)
[![Sonar Maintainability](https://sonarcloud.io/api/project_badges/measure?project=common_ci_scripts_for_dockerhub_release&metric=sqale_rating "Sonar Maintainability")](https://sonarcloud.io/dashboard?id=common_ci_scripts_for_dockerhub_release)
[![Docker Image](https://img.shields.io/docker/image-size/riccardocampisano/public/common_ci_scripts_for_dockerhub_release-latest?label=common_ci_scripts_for_dockerhub_release-latest&logo=docker "Docker Image")](https://hub.docker.com/r/riccardocampisano/public/tags?name=common_ci_scripts_for_dockerhub_release)

# Sample CI/CD project with dockerhub integration

This project shows how to setup a pipeline using CI services like GitLab CI to:
- build and test a (C++/Java/etc) multiarch (amd64/arm64) project using specific docker images;
- produce and send coverage info to sonarcloud.io;
- build a docker image, tag and push it to hub.docker.com.



## CI Rules:

1) push commits in not-master branch:
    - build, test, coverage
0) push commits in master branch:
    - build, test, coverage
    - crossbuild for arm64
    - create a new multiarch docker image, tagged as master and push it to docker hub
0) push tags in master branch:
    - build, test, coverage
    - crossbuild for arm64
    - create a new multiarch docker image, double tagged as current tag and latest, and push it to docker hub



## CI requirements:
- docker repository (hub.docker.com) push access
- sonarcloud.io bind



## Configuration:

1) configure the follows pipeline environment variables:
    - DOCKER_USERNAME
    - DOCKER_PASSWORD
    - DOCKER_REGISTRY_URL
    - SONAR_ORGANIZATION
    - SONAR_TOKEN



    Step by step howto:

        1 - Docker Hub access
          login with your account on hub.docker.com and go to -> Account Settings -> Security
            create a new Access Token to allow CI pipeline to push images into Docker Hub (suggested description: "CI push")
          go to gitlab.com  -> your project -> Settings -> CI/CD -> Variables Expand
            add the follow env vars:
            - DOCKER_REGISTRY_URL (not masked) containing the Docker Hub repository to push images (e.g. riccardocampisano/public)
            - DOCKER_USERNAME     (not masked) containing your Docker Hub username                 (e.g. riccardocampisano)
            - DOCKER_PASSWORD     (masked)     containing the Docker Hub Access Token              (e.g. the one previously created)

        2 - SonarCloud access
          login with your account on sonarcloud.io and go to -> user-icon -> My Account -> Security
            create a new Token to allow CI pipeline to push code analisys into SonarCloud (suggested name: "CI push")
          go to gitlab.com  -> your project -> Settings -> CI/CD -> Variables Expand
            add the follow env vars:
            - SONAR_ORGANIZATION (not masked) containing the SonarCloud organization key  (e.g. campisano)
            - SONAR_TOKEN        (masked)     containing the SonarCloud Token             (e.g. the one previously created)





2) edit the scripts in the `./ci/custom` folder



## Screenshots:



### Required pipeline env vars

![Alt text](/doc/README.md/pipeline-env-vars.png?raw=true "pipeline env vars")



### Resulting pipeline builds

![Alt text](/doc/README.md/pipeline-builds.png?raw=true "pipeline builds")



### Resulting dockerhub images

![Alt text](/doc/README.md/dockerhub-images.png?raw=true "dockerhub images")
