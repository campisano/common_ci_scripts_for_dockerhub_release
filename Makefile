# mandatory vars:

binary_name			:= whoami
type				:= exec



# optional vars:
include_paths			:= src/adapters/inc \
                                   lib/doctest/install/include
library_paths			:= src/adapters/build/lib

source_extension		?= .cpp
source_paths			?= src
libs_to_link			:= :adapters.so

main_source_paths		:= src/main

test_source_paths		:= src/test
test_add_libs_to_link		?=

output_folder			?= build
DESTDIR				?=
PREFIX				:= $(CURDIR)/install

format_dirs			:= src
format_extensions		?= .hpp .cpp

subcomponent_paths		:= src/adapters

include lib/generic_makefile/modular_makefile.mk
